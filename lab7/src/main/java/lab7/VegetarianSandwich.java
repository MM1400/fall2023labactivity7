/**
 * @author Ahmed Sobh
 * Student ID: 2134222
 */

package lab7;

public abstract class VegetarianSandwich implements ISandwich{
    private String filling;
    private final String[] meat = {"chicken", "beef", "fish", "meat" , "pork"};

    public VegetarianSandwich(){
        this.filling = "";
    }

    public void addFilling(String topping){
        for(int i = 0; i < meat.length; i++){
            if(topping.equals(meat[i]))
                throw new IllegalArgumentException("Cannot add a meat to a vegetarian sandwich.");
        }
        filling += topping;
    }
    
    public abstract String getProtein();

    public boolean isVegetarian(){
        return true;
    }

    public boolean isVegan(){
        if(filling.contains("cheese") || filling.contains("eggs")){
            return false;
        }
        return true;
    }
    public String getFilling() {
        return this.filling;
    }
}