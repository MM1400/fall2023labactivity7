package lab7;

public class CaesarSandwich extends VegetarianSandwich{

    public CaesarSandwich() {
        addFilling("Caesar dressing");
    }
    @Override
    public String getProtein() {
        return "Anchovies";
    }
    @Override
    public boolean isVegetarian() {
        return false;
    }
    public String getFilling() {
        return filling;
    }
}
