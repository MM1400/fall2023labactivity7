package lab7;

public class BagelSandwich implements ISandwich {
    private String filling;

    public BagelSandwich() {
        this.filling = "";
    }
    public String getFilling() {
        return this.filling;
    }
    public void addFilling(String topping) {
        this.filling = this.filling + topping;
    }
    public boolean isVegetarian() {
        throw new UnsupportedOperationException("Bagel Sandwich is not vegetarian");
    }
}
