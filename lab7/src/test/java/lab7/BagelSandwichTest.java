package lab7;

import static org.junit.Assert.*;

import org.junit.Test;

public class BagelSandwichTest{

    @Test
    public void testConstructor() {
        BagelSandwich testBagel = new BagelSandwich();
        assertEquals("", testBagel.getFilling());    
    }

    @Test
    public void testAddFilling(){
        BagelSandwich testBagel = new BagelSandwich();

        testBagel.addFilling("meat");

        assertEquals("meat", testBagel.getFilling());
    }


    @Test
    public void testGetFilling(){
        BagelSandwich testBagel = new BagelSandwich();

        assertEquals("", testBagel.getFilling());
    }

    @Test
    public void testIsVegetarian(){
        BagelSandwich testBagel = new BagelSandwich();
        try {
            testBagel.isVegetarian();
            fail("Not vegetarian");
        } catch(UnsupportedOperationException e){
            System.out.println("The Bagel is not vegetarian");
        }; 
    }
}