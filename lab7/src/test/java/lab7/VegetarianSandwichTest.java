package lab7;
import static org.junit.Assert.*;
import org.junit.Test;

public class VegetarianSandwichTest {
    @Test
    public void constructor_emptyString() {
        VegetarianSandwich v = new VegetarianSandwich();
        assertEquals("",v.getFilling());
    }
    @Test
    public void getFilling_returnsCheese() {
            VegetarianSandwich v = new VegetarianSandwich();
            v.addFilling("cheese");
            assertEquals("cheese",v.getFilling());
    }
    @Test
    public void addFilling_exception() {
        try {
            VegetarianSandwich v = new VegetarianSandwich();
            v.addFilling("chicken");
            fail("Was able to add chicken");
        } catch(IllegalArgumentException e) { }
    }
    @Test
    public void isVegetarian_returnsTrue() {
        VegetarianSandwich v = new VegetarianSandwich();
        assertEquals(true, v.isVegetarian());
    }
    @Test
    public void isVegan_returnsFalseIfCheese() {
        VegetarianSandwich v = new VegetarianSandwich();
        v.addFilling("cheese");
        assertEquals(false, v.isVegan());
    }
    @Test
    public void isVegan_returnsTrueIfNoCheeseOrEgg() {
        VegetarianSandwich v = new VegetarianSandwich();
        assertEquals(true, v.isVegan());
    }
}
